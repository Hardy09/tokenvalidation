import jsonwebtoken from "jsonwebtoken";

const generateToken = async () => {
    return jsonwebtoken.sign({courseId: "1", courseTitle: "ABC"},
        'Hardy09xx#', {expiresIn: '1hr'});
}

const res = await generateToken();
console.log(res);
