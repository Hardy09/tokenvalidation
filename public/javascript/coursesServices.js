//import * as LocalStorage from "../../node_modules/node-localstorage";

function coursesServices() {
    let html = '';
    for (let i = 0; i < 10; i++) {
        let btn = document.createElement("input");   // Create a <button> element
        btn.value = "BUTTON";                   // Insert text
        btn.className = "btn btn-primary";
        btn.type = "button";
        let h5 = document.createElement("h5");
        h5.className = "card-title";
        let p = document.createElement("p");
        p.className = "card-text";
        p.innerText = "Some quick example text to build on the card title and make up the bulk of the card's content.";
        let div = document.createElement("div");
        div.className = 'card shadow';
        div.style.margin = '16px';
        div.style.width = '20rem';
        div.style.height = '390px';
        div.style.border = 'none';
        let img = document.createElement("img");
        img.className = "card-img-top";
        img.src = "https://mdbootstrap.com/img/new/standard/nature/184.jpg";
        div.appendChild(img);
        let div1 = document.createElement("div");
        div1.className = "card-body";
        div1.appendChild(h5);
        div1.appendChild(p);
        div1.appendChild(btn);
        div.appendChild(div1);
        document.getElementById('courseView').insertAdjacentElement('afterbegin', div);
    }
}

const tokenError = () => {
    if (typeof localStorage === "undefined" || localStorage === null) {
        let LocalStorage = require('node-localstorage').LocalStorage;
        localStorage = new LocalStorage('./scratch',1);
    }
    console.log("HELLO ",localStorage.getItem('Error'));
    document.querySelector('#tokenError').innerHTML = localStorage.getItem('Error');
}
