export const validateForm = (req, res, next) => {
    console.log("FROM VALIDATE ", req.body);
    try {
        if (req.body.token === '') {
            return res.status(400).send('Token is Empty');
        } else {
            return next();
        }
    }catch (err){
        res.status(400).send(err.message);
    }
}
