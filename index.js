import router from './routes/route.js';
import express from "express";
import bodyParser from 'body-parser';
import {fileURLToPath} from "url";
import path from "path";

const app = express();
const __filename = fileURLToPath(import.meta.url);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(router);
app.use('/public', express.static('public')); // static is used to send static data like css,js, img to the clients.
//app.use(express.static(path.join(path.dirname(__filename),'./public/common.css')));
//app.set("views",'./templates');

app.listen(5000,() => {
    console.log("Listening to port 5000");
});
