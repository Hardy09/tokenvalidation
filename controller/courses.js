import {fileURLToPath} from "url";
import path from "path";

export const courses = async (req, res, next) => {
    const __filename = fileURLToPath(import.meta.url);
    res.sendFile(path.join(path.dirname(__filename), '../templates/Courses.html'));
}
