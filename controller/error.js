import {fileURLToPath} from "url";
import path from "path";

export const error = async (req, res, next, errorMessage) => {
    const __filename = fileURLToPath(import.meta.url);
    // res.json(errorMessage);
    res.sendFile(path.join(path.dirname(__filename), '../templates/Error.html'));
}
