import path from "path";
import { fileURLToPath } from 'url';

export const inputField = async (req, res, next) => {
    const __filename = fileURLToPath(import.meta.url);
    res.sendFile(path.join(path.dirname(__filename), '../templates/index.html'));
}



