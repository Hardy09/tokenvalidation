import {Router} from 'express';
import {inputField} from "../controller/input.js";
import jsonwebtoken from "jsonwebtoken";
import {validateForm} from "../middlewares/validateForm.js";
import {courses} from "../controller/courses.js";
import {error} from "../controller/error.js";
import {LocalStorage} from "node-localstorage";

const verifyToken = async (token) => {
    return jsonwebtoken.verify(token, 'Hardy09xx#');
}

const router = Router();

router.get('/', inputField);

router.post('/validateToken', validateForm, async (req, res, next) => {
    try {
        const verify = await verifyToken(req.body.token);
        console.log(verify);
        await courses(req, res, next);
    } catch (err) {
        console.log("Verify Error ", err.message);
        if (typeof localStorage === "undefined" || localStorage === null) {
            //let LocalStorage = require('node-localstorage').LocalStorage;
            let localStorage = new LocalStorage('./scratch');
            localStorage.setItem('Error', err.message);
        }
        await error(req, res, next, err.message);
    }
});

export default router;
